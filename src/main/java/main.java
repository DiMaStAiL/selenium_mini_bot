import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.*;

public class main {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "D:\\Java pro\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver(); //Создаем обьек драйвера хром
        driver.manage().window().maximize();    //устанавливаем размер окна на весь размер
//      driver.manage().window().setSize(new Dimension(1920, 1080));//устанавливает определенный размер окна

        //#1 тест

//        переход на сайт : 1й способ
//        driver.get("http://google.com");

//        //переход на сайт 2й способ
//        driver.navigate().to("http://google.com");
//        WebElement obj = driver.findElement(By.name("q"));
//        obj.sendKeys("ноутбуки");
//        obj.sendKeys(Keys.ENTER);
//        List<WebElement> site = driver.findElements(By.className("TbwUpd"));
//        for (WebElement i: site) {
//            System.out.println(i.getText());
//        }

        //#2 небольшой бот на поиск максимальной цены товара
        String link = "https://rozetka.com.ua";  //сайт для поиска

        driver.get(link); //загружаем сайт
        WebElement obj = driver.findElement(By.name("search"));//находим елемент по имени тега
        obj.sendKeys("ноутбуки");//тыкаем на него и вставляем текст
        obj.sendKeys(Keys.ENTER);//отправляем ентр

        Thread.sleep(60000);//ждем 1 минуту для загрузки страницы ( у меня не быстый инет)

        List<WebElement> product_name = driver.findElements(By.className("goods-tile__title"));//находим уже список елементов
        ArrayList<String> product = new ArrayList<String>(); //в этот контейнер будут занесены названия товаров
        for (WebElement i: product_name) { //перебераем веб елементы и вытаскиваем из них текст
            product.add(i.getText());//метод add добавляет в конец елемент
        }

        List<WebElement> product_price = driver.findElements(By.className("goods-tile__price-value"));//тут будут уже цены
        ArrayList<Double> product_ = new ArrayList<Double>();

        for (WebElement i: product_price) {
            product_.add( Double.parseDouble(  i.getText().replaceAll("[ ]", "") ));
            //для преобразования строки в число, нужно удалить пробел, так как числа на сайте с проелами
        }

        Map<String, Double> full_list_product = new HashMap<String, Double>();
        //мап для создания контейнера "названия - цена"

        for (int i =0; i < product.size(); i++){
            full_list_product.put(product.get(i), product_.get(i));
            //для мап уже метод добавления put
        }

        //находим максимальное число
        Double max = -1.0;
        String key = "";
        for (Map.Entry<String, Double> i: full_list_product.entrySet()) {
                if (i.getValue() > max){//если текущий макс, меньше числа в контейнере, берем число контейнера + забераем его ключ
                    max = i.getValue();
                    key = i.getKey();
                }
        }

        System.out.println(key + " " + full_list_product.get(key));
        //через метод гет и ключ, выводим найденый елемент

    }
}
